CREATE TABLE query_result
(
tracking_id VARCHAR(255),
visit_date VARCHAR(255),
ga_adKeywordMatchType VARCHAR(255),
ga_adMatchedQuery VARCHAR(255),
ga_browser VARCHAR(255),
ga_browserSize VARCHAR(255),
ga_deviceCategory VARCHAR(255),
ga_fullReferrer VARCHAR(255),
ga_latitude FLOAT,
ga_longitude FLOAT,
ga_medium VARCHAR(255),
ga_mobileDeviceBranding VARCHAR(255),
ga_operatingSystem VARCHAR(255),
ga_source VARCHAR(255),
aggregated_campaign VARCHAR(255),
time_in_crm VARCHAR(255),
budget_amount_value Float,
Avaluo_pagado Float,
Ganado INT,
Perdido INT
);


LOAD DATA LOCAL INFILE
'/Users/yogesh.yadav/YogeshProjects/ml_modeling/resources/query_result.csv'
INTO TABLE query_result
CHARACTER SET latin1
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;

select count(*) from query_result; -- 762891
select count(DISTINCT tracking_id) from query_result; -- 498532

-- Number of duplicates = 264359
-- Percentage of duplicates = 34% ( Quite a big number ) . You should dig dipper to find out why you are getting duplicate data  *** Note 1

select * from query_result
where tracking_id in (
select tracking_id
from query_result a
group by tracking_id
having count(*)>1 )
order by tracking_id

-- For the same tracking id , visit_date is different , so basically its the same customer who visit the website again.
-- So , Lets do this simple calculation
-- Total count duplicates = 264359

select tracking_id, count(*) from query_result
where tracking_id in (
select tracking_id
from query_result a
group by tracking_id
having count(*)>1)
group by tracking_id
order by tracking_id;


select count(*) as total_repeted_session, count(distinct tracking_id) number_of_repeted_customer from query_result
where tracking_id in (
select tracking_id
from query_result a
group by tracking_id
having count(*)>1)
-- total_repeted_session    number_of_repeted_customer
-- 380066	                115707

-- Note 0 , 380066/115707 = 3.284 So , this number is telling me that if somebody is coming back  he comes back more than 3 times, which is a good thing

--- Note 1 , so its not duplicate data , it is basically telling us that if 100 new session happen on the website , 34 more session will happen for sure

select ga_deviceCategory, count(*) from query_result
where time_in_crm is not null
and Avaluo_pagado != 0 and `Avaluo_pagado` is not null
group By ga_deviceCategory;

select * from query_result where Ganado = 1 and Perdido = 1;
select * from query_result where `Avaluo_pagado` is not null ;


select count(DISTINCT tracking_id) from query_result; -- 498532
select count(DISTINCT tracking_id) from query_result where time_in_crm is not null; -- 20940
select * from query_result where time_in_crm is not null and budget_amount_value is not null group by tracking_id; -- 20380
select count(DISTINCT tracking_id) from query_result where time_in_crm is not null and budget_amount_value is not null and `Avaluo_pagado` = 1;






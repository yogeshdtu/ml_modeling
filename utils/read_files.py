import json


def read_json(filename, json_property=None):
    with open(filename) as json_file:
        json_data = json.load(json_file, encoding='utf-8')
        if json_property is not None:
            return json_data[json_property]
        else:
            return json_data

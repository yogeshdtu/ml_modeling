import pandas
import logging
import importlib
import sys
from configs.validators import config
from validators.validator_factory import ValidatorFactory

logger = logging.getLogger(__name__)


class ETL:
    def __init__(self):
        self.config = config
        self.refactored_data = None
        self.data = self.read_data_file()

    def read_data_file(self):
        try:
            return pandas.read_csv(
                self.config['filename'], sep=',', chunksize=self.config['chunksize'],
                nrows=100, usecols=self.config['columns_needed']
            )
        except Exception as e:
            logger.critical(str(e.message))
            sys.exit("Error in reading file. Please check if {} is present or not".format(self.config['filename']))

    def load_validators(self):
        validator_factory = ValidatorFactory()
        if self.config['validate']:
            for validator_name, arguments in self.config['validators'].iteritems():
                if arguments['apply']:
                    module_name, class_name = arguments['handler'].strip().split('|')
                    imported_module = importlib.import_module(module_name)
                    validator_factory.add_validator(getattr(imported_module, class_name)(validator_name))

        return validator_factory.get_validators()

    # todo Increase chunksize
    def clean_data(self):
        logger.info("Cleaning Data..")
        clean_df = pandas.DataFrame(columns=self.config['columns_needed'])
        validators = self.load_validators()
        id = 0
        response = True

        for rdf in self.data:
            logger.debug("Checking id {}".format(str(id)))
            for validator in validators:
                response = validator.validate(rdf)
                if not response:
                    #logger.debug("Skipping id {}".format(str(id)))
                    break

            if response:
                #logger.debug("Adding id {}".format(str(id)))
                clean_df = clean_df.append(rdf)

            id += 1
        logger.info("Removing duplicates")
        clean_df = clean_df.drop_duplicates(['tracking_id'], keep='first')

        return clean_df

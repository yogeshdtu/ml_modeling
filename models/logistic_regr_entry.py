# -*- coding: utf-8 -*-
import pandas
import logging
from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle
from sklearn.linear_model import LogisticRegression

logger = logging.getLogger(__name__)


class Filter:
    def __init__(self):
        logger.info("Filtering Data ................................")
        self.filename = 'resources/df_output.csv'

    def etl(self):
        logger.info("Reading Data file")
        raw_data = pandas.read_csv(self.filename, sep=',', low_memory=False, encoding='utf-8')
        logger.info("Data Size => Rows {} , Cols {}".format(str(raw_data.shape[0]), str(raw_data.shape[1])))

        # Remove duplicates
        logger.info("Removing duplicates....")
        process_data = raw_data.drop_duplicates(subset=['tracking_id', 'visit_date'])
        logger.info("Data Size => Rows {} , Cols {}".format(str(process_data.shape[0]), str(process_data.shape[1])))

        logger.info("Removing rows where critical information is Nan")

        process_data = process_data.dropna(how='any',
                                           subset=['tracking_id'])
        logger.info("Data Size => Rows {} , Cols {}".format(str(process_data.shape[0]), str(process_data.shape[1])))

        logger.info("Fixing data values")
        logger.info("Or we can do percentage wise distribution")  # todo
        process_data = process_data.fillna(value={'ga_adMatchedQuery': '_unknown', 'ga_browser': '_unknown',
                                                  'ga_deviceCategory': '_unknown', 'ga_latitude': 0, 'ga_longitude': 0,
                                                  'ga_medium': '_unknown', 'ga_mobileDeviceBranding': '_unknown',
                                                  'ga_operatingSystem': '_unknown',
                                                  'ga_source': '_unknown', 'aggregated_campaign': '_unknown'
                                                  })

        logger.info("Data Size => Rows {} , Cols {}".format(str(process_data.shape[0]), str(process_data.shape[1])))

        logger.info("Filtering garbage data from browser column")
        process_data.loc[process_data['ga_browser'].str.contains(';__'), 'ga_browser'] = '_unknown'

        logger.info("Shuffling for proper distribution of train and test dataset.")
        return shuffle(process_data)


class Analysis:
    def __init__(self, df):
        logger.info("Analysing data")
        logger.info(
            "Most of the analysis is done via mysql. Please see the word file attached. Just modifying data here")
        self.df = df

    # todo add visit count , keep the row where it reach to time_to_crm if present, if the customer is coming for the second time , the chances of conversion increases.
    def modify(self):
        return self.df


class LogisticRegr:
    def __init__(self, df):
        logger.info("Decision Tree Classifier ................................")
        self.data = df
        self.all_cols = ["visit_month", "ga_browser", "ga_deviceCategory",
                         "ga_fullReferrer",
                         "ga_medium", "ga_mobileDeviceBranding",
                         "ga_operatingSystem", "ga_source",
                         "aggregated_campaign"]

        self.columns_category = ["ga_browser", "ga_deviceCategory",
                                 "ga_fullReferrer",
                                 "ga_medium", "ga_mobileDeviceBranding",
                                 "ga_operatingSystem", "ga_source",
                                 "aggregated_campaign"]

    def exilir(self):
        logger.info("Converting 'time_in_crm' into binary classes for classification.")
        self.data.time_in_crm.loc[~self.data.time_in_crm.isnull()] = 1
        self.data.time_in_crm.loc[self.data.time_in_crm.isnull()] = 0

        logger.info("Creating new feature month from visit_date")
        self.data['visit_month'] = pandas.DatetimeIndex(self.data['visit_date']).month

        data_X = self.data[self.all_cols]
        for col in self.columns_category:
            data_X[col] = data_X[col].astype('category').cat.codes.values

        data_y = self.data[['time_in_crm']]

        return data_X.values, data_y.values

    def logistic_regr(self):
        logger.info("Transforming data into required matrices...")
        data_X, data_y = self.exilir()
        assert (data_X.shape[0] == data_y.shape[0]), "Input rows count are not equal to output rows count"

        logger.info("Splitting for train and test")
        X_train, X_test, y_train, y_test = train_test_split(data_X, data_y, test_size=0.25,
                                                            random_state=21)

        logger.info("Train data size = {}. Number of features = {}".format(str(X_train.shape[0]), str(X_train.shape[1])))
        logger.info("Test data size = {}".format(X_test.shape[0]))

        logger.info("Initiating Logistic Regression Classifier")
        lr = LogisticRegression()

        logger.info("Training Logistic Regression Classifier")

        y_train = y_train.ravel()
        y_test = y_test.ravel()

        lr.fit(X_train, y_train)

        logger.info("Predicting test data set")
        y_predict = lr.predict(X_test)

        match = 0
        for i in xrange(0, y_predict.shape[0]):
            if y_predict[i] == y_test[i]:
                match += 1

        logger.info("Match = {}, Total = {}, accuracy = {}".format(str(match), str(y_predict.shape[0]), str(match*100.0/y_predict.shape[0])))

        score = lr.score(X_test, y_test.reshape(-1, 1))
        logger.info("Score = {}".format(str(score)))


def run():
    logger.info("Making model for Time in crm. Entry funnel. Case 1")
    processed_data = Filter().etl()
    modified_data = Analysis(processed_data).modify()
    logger.info("Running logistic regression")
    LogisticRegr(modified_data).logistic_regr()
    logger.info("|---------< Finish Run >---------|")

# -*- coding: utf-8 -*-
import pandas
import logging
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score
from sklearn import svm
from sklearn import tree
from sklearn.utils import shuffle
import graphviz

logger = logging.getLogger(__name__)


class Filter:
    def __init__(self):
        logger.info("Filtering Data ................................")
        self.initial_filename = 'resources/query_result.csv'
        self.final_filename = 'resources/df_output.csv'

    def etl(self):
        logger.info("Reading Data file")
        latin_data = pandas.read_csv(self.initial_filename, sep=',', low_memory=False, encoding='latin1')

        latin_data.to_csv(self.final_filename, encoding='utf8')

        raw_data = pandas.read_csv(self.final_filename, sep=',', low_memory=False, encoding='utf8')

        logger.info("Data Size => Rows {} , Cols {}".format(str(raw_data.shape[0]), str(raw_data.shape[1])))

        # Remove duplicates
        logger.info("Removing duplicates....")
        process_data = raw_data.drop_duplicates(subset=['tracking_id', 'visit_date'])
        logger.info("Data Size => Rows {} , Cols {}".format(str(process_data.shape[0]), str(process_data.shape[1])))

        logger.info("Removing rows where critical information is Nan")

        '''
        process_data = process_data.dropna(how='any',
                                           subset=['tracking_id', 'time_in_crm', 'budget_amount_value',
                                                   'Avalúo pagado'])
        '''

        process_data = process_data.dropna(how='any',
                                           subset=['tracking_id'])
        logger.info("Data Size => Rows {} , Cols {}".format(str(process_data.shape[0]), str(process_data.shape[1])))

        '''
        logger.info("Fixing data types")
        process_data['Avalúo pagado'] = process_data['Avalúo pagado'].astype(dtype='int')
        process_data['Ganado'] = process_data['Ganado'].astype(dtype='int')
        process_data['Perdido'] = process_data['Perdido'].astype(dtype='int')
        logger.info("Data Size => Rows {} , Cols {}".format(str(process_data.shape[0]), str(process_data.shape[1])))
        '''

        logger.info("Fixing data values")
        logger.info("Or we can do percentage wise distribution")  # todo
        process_data = process_data.fillna(value={'ga_adMatchedQuery': '_unknown', 'ga_browser': '_unknown',
                                                  'ga_deviceCategory': '_unknown', 'ga_latitude': 0, 'ga_longitude': 0,
                                                  'ga_medium': '_unknown', 'ga_mobileDeviceBranding': '_unknown',
                                                  'ga_operatingSystem': '_unknown',
                                                  'ga_source': '_unknown', 'aggregated_campaign': '_unknown'
                                                  })
        logger.info("Data Size => Rows {} , Cols {}".format(str(process_data.shape[0]), str(process_data.shape[1])))

        logger.info("Filtering garbage data from browser column")
        process_data.loc[process_data['ga_browser'].str.contains(';__'), 'ga_browser'] = '_unknown'

        logger.info("Shuffling for proper distribution of train and test dataset.")
        return shuffle(process_data)


class Analysis:
    def __init__(self, df):
        logger.info("Analysing data ................................")
        logger.info(
            "Most of the analysis is done via mysql. Please see the word file attached. Just modifying data here")
        self.df = df

    # todo add visit count , keep the row where it reach to time_to_crm if present, if the customer is coming for the second time , the chances of conversion increases.
    # todo grouping of latitude and longitude into cities
    def modify(self):
        logger.info("Adding visit count")
        return self.df
        # modified_data =  self.df.group_by()


class Knn:
    def __init__(self, df):
        self.data = df
        self.all_cols = ["ga_browser", "ga_deviceCategory",
                         "ga_fullReferrer",
                         "ga_latitude", "ga_longitude",
                         "ga_medium", "ga_mobileDeviceBranding",
                         "ga_operatingSystem", "ga_source",
                         "aggregated_campaign"]

        self.columns_category = ["ga_browser", "ga_deviceCategory",
                                 "ga_fullReferrer",
                                 "ga_medium", "ga_mobileDeviceBranding",
                                 "ga_operatingSystem", "ga_source",
                                 "aggregated_campaign"]

    def exilir(self):
        self.data.time_in_crm.loc[~self.data.time_in_crm.isnull()] = 1
        self.data.time_in_crm.loc[self.data.time_in_crm.isnull()] = 0
        data_X = self.data[self.all_cols]
        for col in self.columns_category:
            data_X[col] = data_X[col].astype('category').cat.codes.values

        data_y = self.data[['time_in_crm']]

        return data_X.values, data_y.values

    def k_nearest(self):
        logger.info("Transforming data into required matrices...")
        data_X, data_y = self.exilir()
        assert (data_X.shape[0] == data_y.shape[0]), "Input rows count are not equal to output rows count"

        logger.info("Splitting for train and test")
        X_train, X_test, y_train, y_test = train_test_split(data_X, data_y, test_size=0.35,
                                                            random_state=81)

        logger.info(
            "Train data size = {}. Number of features = {}".format(str(X_train.shape[0]), str(X_train.shape[1])))
        logger.info("Test data size = {}".format(X_test.shape[0]))

        knn = KNeighborsClassifier()
        knn.fit(X_train, y_train)
        y_predict = knn.predict(X_test)

        match = 0
        for i in xrange(0, y_predict.shape[0]):
            if y_predict[i] == y_test[i][0]:
                match += 1

        logger.info("Match = {}, Total = {}, accuracy = {}".format(str(match), str(i), str(match * 100.0 / i)))

        logger.info("match = {}, total = {}, accuracy = {}".format(str(match), str(i), str(match * 100.0 / i)))


# Will not give goo result
class LR:
    def __init__(self, df):
        self.data = df
        self.all_cols = ["ga_browser", "ga_deviceCategory",
                         "ga_fullReferrer",
                         "ga_latitude", "ga_longitude",
                         "ga_medium", "ga_mobileDeviceBranding",
                         "ga_operatingSystem", "ga_source",
                         "aggregated_campaign"]

        self.columns_category = ["ga_browser", "ga_deviceCategory",
                                 "ga_fullReferrer",
                                 "ga_medium", "ga_mobileDeviceBranding",
                                 "ga_operatingSystem", "ga_source",
                                 "aggregated_campaign"]

    def exilir(self):
        self.data.time_in_crm.loc[~self.data.time_in_crm.isnull()] = 1
        self.data.time_in_crm.loc[self.data.time_in_crm.isnull()] = 0
        data_X = self.data[self.all_cols]
        for col in self.columns_category:
            data_X[col] = data_X[col].astype('category').cat.codes.values

        data_y = self.data[['time_in_crm']]

        return data_X, data_y

    def linear_reg(self):
        data_X, data_y = self.exilir()

        assert (data_X.shape[0] == data_y.shape[0]), "Input rows count are not equal to output rows count"

        logger.info("Converting to ndarrays and Splitting for train and test")
        X_train, X_test, y_train, y_test = train_test_split(data_X.values, data_y.values, test_size=0.30,
                                                            random_state=21)

        lr = linear_model.LinearRegression()
        lr.fit(X_train, y_train)

        return lr.predict(X_test)


# Will take too much time. Need to normalise also
class SVM_Kernel:
    def __init__(self, df):
        self.data = df
        self.all_cols = ["ga_browser", "ga_deviceCategory",
                         "ga_fullReferrer",
                         "ga_latitude", "ga_longitude",
                         "ga_medium", "ga_mobileDeviceBranding",
                         "ga_operatingSystem", "ga_source",
                         "aggregated_campaign"]

        self.columns_category = ["ga_browser", "ga_deviceCategory",
                                 "ga_fullReferrer",
                                 "ga_medium", "ga_mobileDeviceBranding",
                                 "ga_operatingSystem", "ga_source",
                                 "aggregated_campaign"]

    def exilir(self):
        self.data.time_in_crm.loc[~self.data.time_in_crm.isnull()] = 1
        self.data.time_in_crm.loc[self.data.time_in_crm.isnull()] = 0
        data_X = self.data[self.all_cols]
        for col in self.columns_category:
            data_X[col] = data_X[col].astype('category').cat.codes.values

        data_y = self.data[['time_in_crm']]

        return data_X, data_y

    def svm_kernel(self):
        data_X, data_y = self.exilir()

        assert (data_X.shape[0] == data_y.shape[0]), "Input rows count are not equal to output rows count"

        logger.info("Converting to ndarrays and Splitting for train and test")
        X_train, X_test, y_train, y_test = train_test_split(data_X.values, data_y.values, test_size=0.30,
                                                            random_state=3)

        svc = svm.SVC(kernel='linear', cache_size=8192)

        svc.fit(X_train, y_train)

        return svc.predict(X_test)


# Good for analysing , can analyse from here and try running on different model.
class DTree:
    def __init__(self, df):
        logger.info("Decision Tree Classifier ................................")
        self.data = df
        self.all_cols = ["visit_month", "ga_browser", "ga_deviceCategory",
                         "ga_fullReferrer",
                         "ga_latitude", "ga_longitude",
                         "ga_medium", "ga_mobileDeviceBranding",
                         "ga_operatingSystem", "ga_source",
                         "aggregated_campaign"]

        logger.info("Features Used {}".format(', '.join(self.all_cols)))

        self.columns_category = ["ga_browser", "ga_deviceCategory",
                                 "ga_fullReferrer",
                                 "ga_medium", "ga_mobileDeviceBranding",
                                 "ga_operatingSystem", "ga_source",
                                 "aggregated_campaign"]

    def exilir(self):
        logger.info("Converting 'time_in_crm' into binary classes for classification.")
        self.data.time_in_crm.loc[~self.data.time_in_crm.isnull()] = 1
        self.data.time_in_crm.loc[self.data.time_in_crm.isnull()] = 0

        logger.info("Creating new feature month from visit_date")
        self.data['visit_month'] = pandas.DatetimeIndex(self.data['visit_date']).month

        logger.info("Categorizing features")
        data_X = self.data[self.all_cols]
        for col in self.columns_category:
            data_X[col] = data_X[col].astype('category').cat.codes.values

        data_y = self.data[['time_in_crm']]

        return data_X.values, data_y.values

    def dtree(self):
        logger.info("Transforming data into required matrices...")
        data_X, data_y = self.exilir()
        assert (data_X.shape[0] == data_y.shape[0]), "Input rows count are not equal to output rows count"

        logger.info("Splitting for train and test")
        X_train, X_test, y_train, y_test = train_test_split(data_X, data_y, test_size=0.25,
                                                            random_state=21)

        logger.info(
            "Train data size = {}. Number of features = {}".format(str(X_train.shape[0]), str(X_train.shape[1])))
        logger.info("Test data size = {}".format(X_test.shape[0]))

        logger.info("Initiating decision tree classifier")
        dt = tree.DecisionTreeClassifier(max_depth=30)

        logger.info("Training decision tree classifier")
        dt.fit(X_train, y_train)

        logger.info("Predicting test data set")
        y_predict = dt.predict(X_test)

        logger.info("Converting decision tree into pdf for analysis")



        match = 0
        for i in xrange(0, y_predict.shape[0]):
            if y_predict[i] == y_test[i][0]:
                match += 1

        logger.info("Match = {}, Total = {}, accuracy = {}".format(str(match), str(y_predict.shape[0]),
                                                                   str(match * 100.0 / y_predict.shape[0])))

        logger.info("The function return probability of each test case for the binary class")
        return dt.predict_proba(X_test)


def run():
    logger.info("Making model for time_in_crm.. Case 1")
    processed_data = Filter().etl()
    modified_data = Analysis(processed_data).modify()
    logger.info("Running Decision Tree")
    DTree(modified_data).dtree()
    logger.info("|---------< Finish Run >---------|")

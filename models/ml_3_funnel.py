# -*- coding: utf-8 -*-
import pandas
import logging
from sklearn.model_selection import train_test_split
from sklearn import tree
from sklearn.utils import shuffle
import graphviz

logger = logging.getLogger(__name__)


class Filter:
    def __init__(self):
        logger.info("Filtering Data ................................")
        self.filename = 'resources/df_output.csv'

    def etl(self):
        logger.info("Reading Data file")
        raw_data = pandas.read_csv(self.filename, sep=',', low_memory=False, encoding='utf-8')
        logger.info("Data Size => Rows {} , Cols {}".format(str(raw_data.shape[0]), str(raw_data.shape[1])))

        # Remove duplicates
        logger.info("Removing duplicates....")
        process_data = raw_data.drop_duplicates(subset=['tracking_id', 'visit_date'])
        logger.info("Data Size => Rows {} , Cols {}".format(str(process_data.shape[0]), str(process_data.shape[1])))

        logger.info("Removing rows where critical information is Nan")

        '''
        process_data = process_data.dropna(how='any',
                                           subset=['tracking_id', 'time_in_crm', 'budget_amount_value',
                                                   'Avalúo pagado'])
        '''

        process_data = process_data.dropna(how='any',
                                           subset=['tracking_id'])
        logger.info("Data Size => Rows {} , Cols {}".format(str(process_data.shape[0]), str(process_data.shape[1])))


        '''
        logger.info("Fixing data types")
        process_data['Avalúo pagado'] = process_data['Avalúo pagado'].astype(dtype='int')
        process_data['Ganado'] = process_data['Ganado'].astype(dtype='int')
        process_data['Perdido'] = process_data['Perdido'].astype(dtype='int')
        logger.info("Data Size => Rows {} , Cols {}".format(str(process_data.shape[0]), str(process_data.shape[1])))
        '''

        logger.info("Fixing data values")
        logger.info("Or we can do percentage wise distribution")  # todo
        process_data = process_data.fillna(value={'ga_adMatchedQuery': '_unknown', 'ga_browser': '_unknown',
                                                  'ga_deviceCategory': '_unknown', 'ga_latitude': 0, 'ga_longitude': 0,
                                                  'ga_medium': '_unknown', 'ga_mobileDeviceBranding': '_unknown',
                                                  'ga_operatingSystem': '_unknown',
                                                  'ga_source': '_unknown', 'aggregated_campaign': '_unknown'
                                                  })
        logger.info("Data Size => Rows {} , Cols {}".format(str(process_data.shape[0]), str(process_data.shape[1])))

        logger.info("Filtering garbage data from browser column")
        process_data.loc[process_data['ga_browser'].str.contains(';__'), 'ga_browser'] = '_unknown'

        logger.info("Shuffling for proper distribution of train and test dataset.")
        return shuffle(process_data)


class Analysis:
    def __init__(self, df):
        logger.info("Analysing data")
        logger.info(
            "Most of the analysis is done via mysql. Please see the word file attached. Just modifying data here")
        self.df = df

    # todo add visit count , keep the row where it reach to time_to_crm if present, if the customer is coming for the second time , the chances of conversion increases.
    def modify(self):
        logger.info("Adding visit count")
        return self.df
        # modified_data =  self.df.group_by()


# Good for analysing , can analyse from here and try running on different model.
class DTree:
    def __init__(self, df):
        logger.info("Decision Tree Classifier ................................")
        self.data = df
        self.all_cols = ["visit_month", "ga_browser", "ga_deviceCategory",
                         "ga_fullReferrer",
                         "ga_latitude", "ga_longitude",
                         "ga_medium", "ga_mobileDeviceBranding",
                         "ga_operatingSystem", "ga_source",
                         "aggregated_campaign", "time_in_crm", "budget_amount_value", u"Avalúo pagado"]

        self.columns_category = ["ga_browser", "ga_deviceCategory",
                                 "ga_fullReferrer",
                                 "ga_medium", "ga_mobileDeviceBranding",
                                 "ga_operatingSystem", "ga_source",
                                 "aggregated_campaign"]

    def exilir(self):
        logger.info("Converting 'time_in_crm' into binary classes for classification.")
        self.data.time_in_crm.loc[~self.data.time_in_crm.isnull()] = 1
        self.data.time_in_crm.loc[self.data.time_in_crm.isnull()] = 0

        logger.info("Converting null into 0 to convert into binary classes for classification.")
        self.data['budget_amount_value'].loc[self.data['budget_amount_value'].isnull()] = 0
        self.data['Ganado'].loc[self.data['Ganado'].isnull()] = 0

        logger.info("Creating new feature month from visit_date")
        self.data['visit_month'] = pandas.DatetimeIndex(self.data['visit_date']).month

        self.data[u'Avalúo pagado'].loc[self.data[u'Avalúo pagado'].isnull()] = 0


        data_X = self.data[self.all_cols]
        for col in self.columns_category:
            data_X[col] = data_X[col].astype('category').cat.codes.values

        data_y = self.data[['Ganado']]

        return data_X.values, data_y.values

    def dtree(self):
        logger.info("Transforming data into required matrices...")
        data_X, data_y = self.exilir()
        assert (data_X.shape[0] == data_y.shape[0]), "Input rows count are not equal to output rows count"

        logger.info("Splitting for train and test")
        X_train, X_test, y_train, y_test = train_test_split(data_X, data_y, test_size=0.25,
                                                            random_state=21)

        logger.info("Train data size = {}. Number of features = {}".format(str(X_train.shape[0]), str(X_train.shape[1])))
        logger.info("Test data size = {}".format(X_test.shape[0]))

        logger.info("Initiating decision tree classifier")
        dt = tree.DecisionTreeClassifier(max_depth=55)

        logger.info("Training decision tree classifier")
        dt.fit(X_train, y_train)

        logger.info("Predicting test data set")
        y_predict = dt.predict(X_test)

        # todo need to put labels
        logger.info("Converting decision tree into pdf for analysis")
        try:
            dot_data = tree.export_graphviz(dt, out_file=None)
            graph = graphviz.Source(dot_data)
            graph.render("dt_cj_customer")
        except Exception:
            logger.error("Unable to create pdf , probably because graphviz is not installed.")

        match = 0
        for i in xrange(0, y_predict.shape[0]):
            if y_predict[i] == y_test[i][0]:
                match += 1

        logger.info("Match = {}, Total = {}, accuracy = {}".format(str(match), str(y_predict.shape[0]), str(match*100.0/y_predict.shape[0])))

        logger.info("The function return probability of each test case for the binary class")
        return dt.predict_proba(X_test)


def run():
    logger.info("Making model for Ganado.. Case 3")
    processed_data = Filter().etl()
    modified_data = Analysis(processed_data).modify()
    logger.info("Running Decision Tree")
    DTree(modified_data).dtree()
    logger.info("|---------< Finish Run >---------|")

from abstract_validator import AbstractValidator
import logging
from configs.validators import config

logger = logging.getLogger(__name__)


class MoneyValidator(AbstractValidator):
    def __init__(self, param):
        logger.debug("Initiating Money Validator")
        self.config = config['validators'][param]

    def validate(self, element):
        for column_name in self.config['columns']:
            current = element[column_name].values[0]
            if not self.config['range'][0] <= current <= self.config['range'][1]:
                logger.debug("Money '{}' not in range for current row.".format(str(current)))
                return self.fix(element)

        return True

    def fix(self, element):
        if self.config['apply_fix']:
            for column_name in self.config['columns']:
                element[column_name].values[0] = self.config['fix']
                return True

        else:
            return False

from abstract_validator import AbstractValidator
import pandas
import logging

logger = logging.getLogger(__name__)


class NoneValidator(AbstractValidator):
    def __init__(self, param):
        logger.debug("Initiating None Validator")

    def validate(self, element):
        for column_name in self.config['columns']:
            current = element[column_name].values[0]
            if pandas.isnull(current):
                return self.fix(element)

        return True

    def fix(self, element):
        if self.config['apply_fix']:
            for column_name in self.config['columns']:
                element[column_name].values[0] = self.config['fix']
                return True

        else:
            return False

import logging

logger = logging.getLogger(__name__)


class ValidatorFactory:
    def __init__(self):
        logger.debug("Initiating Validator Factory.")
        self.validators = []

    def add_validator(self, validator):
        self.validators.append(validator)

    def get_validators(self):
        return self.validators

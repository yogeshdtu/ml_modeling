from abstract_validator import AbstractValidator
import logging
from configs.validators import config

logger = logging.getLogger(__name__)


class LatitudeValidator(AbstractValidator):
    def __init__(self, param):
        logger.debug("Initiating Latitude Validator")
        self.config = config['validators'][param]

    def validate(self, element):
        logger.debug(list(element.values))
        for column_name in self.config['columns']:
            current = element[column_name].values[0]
            for min, max in self.config['range']:
                if not min <= current <= max:
                    logger.debug("Latitude not in range for current row.")
                    return self.fix(element)

        return True

    def fix(self, element):
        if self.config['apply_fix']:
            for column_name in self.config['columns']:
                element[column_name].values[0] = self.config['fix'][0][0] # todo for more than one dynamic range
                return True

        else:
            return False

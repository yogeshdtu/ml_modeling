from abstract_validator import AbstractValidator
import logging
import pandas
from configs.validators import config
from datetime import datetime

logger = logging.getLogger(__name__)


class DatetimeValidator(AbstractValidator):
    def __init__(self, param):
        logger.debug("Initiating Date Validator")
        self.config = config['validators'][param]
        self.dateformat = '%Y-%m-%d %H:%M:%S'

    def validate(self, element):
        for column_name in self.config['columns']:
            if pandas.isnull(element[column_name].values[0]):
                return self.fix(element)
        from_date = datetime.strptime(self.config['range'][0], self.dateformat)
        if self.config['use_current_date']:
            to_date = datetime.now()
        else:
            to_date = datetime.strptime(self.config['range'][1], self.dateformat)
        for column_name in self.config['columns']:
            current = datetime.strptime(element[column_name].values[0], self.dateformat)
            if not from_date <= current <= to_date:
                return self.fix(element)

        return True

    def fix(self, element):
        if self.config['apply_fix']:
            for column_name in self.config['columns']:
                element[column_name] = element[column_name].astype('str')
                element[column_name].values[0] = self.config['fix']
                return True

        else:
            return False

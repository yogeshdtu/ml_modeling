from abstract_validator import AbstractValidator
import logging
from configs.validators import config

logger = logging.getLogger(__name__)


class LongitudeValidator(AbstractValidator):
    def __init__(self, param):
        logger.debug("Initiating Longitude Validator")
        self.config = config['validators'][param]

    def validate(self, element):
        for column_name in self.config['columns']:
            current = element[column_name].values[0]
            for min, max in self.config['range']:
                if not min <= current <= max:
                    logger.debug("Longitude not in range for current row.")
                    return self.fix(element)

        return True

    def fix(self, element):
        if self.config['apply_fix']:
            for column_name in self.config['columns']:
                element[column_name].values[0] = self.config['fix'][0][0]
                return True

        else:
            return False

import abc
import logging

logger = logging.getLogger(__name__)


class AbstractValidator:
    __metaclass__ = abc.ABCMeta

    def __init__(self):
        logger.debug("Initiating Abstract Class for Validation rules.")

    @abc.abstractmethod
    def validate(self, element):
        return False

    def fix(self, element):
        return

# todo remove apply param
config = {
    "comment": "Needs to put handles in service files.",
    "columns_needed1": ["tracking_id", "visit_date", "ga_adKeywordMatchType", "ga_adMatchedQuery", "ga_browser",
                        "ga_browserSize", "ga_deviceCategory", "ga_fullReferrer", "ga_latitude", "ga_longitude",
                        "ga_medium", "ga_mobileDeviceBranding", "ga_operatingSystem", "ga_source",
                        "aggregated_campaign", "time_in_crm", "budget_amount_value", "Aval\xfao pagado", "Ganado",
                        "Perdido"],
    "columns_needed": ["tracking_id", "visit_date", "ga_latitude", "ga_longitude", "ga_operatingSystem", "time_in_crm",
                       "budget_amount_value", "Aval\xfao pagado", "Ganado", "Perdido"],
    "filename": "resources/query_result.csv",
    "parse_dates": ["visit_date", "time_in_crm"],
    "chunksize": 1,
    "validate": True,
    "validators": {
        "latitude": {
            "apply": True,
            "columns": ["ga_latitude"],
            "range": [[14.53, 32.7]],
            "apply_fix": True,
            "fix": [[69]],  # Mexico City
            "handler": "validators.latitude_validator|LatitudeValidator"
        },
        "longitude": {
            "apply": True,
            "columns": ["ga_longitude"],
            "range": [[-117.17, -86.78]],
            "apply_fix": True,
            "fix": [[-81]],  # Mexico City
            "handler": "validators.longitude_validator|LongitudeValidator"
        },
        "visit_date": {
            "apply": False,
            "apply_fix": False,
            "use_current_date": True,
            "columns": ["visit_date"],
            "range": ["2012-01-01 00:00:00", "2020-01-01 00:00:00"],
            "fix": "2018-07-26 01:36",  # '%Y-%m-%d %H:%M:%S'
            "handler": "validators.datetime_validator|DatetimeValidator"
        },
        "time_in_crm": {
            "apply": False,
            "apply_fix": False,
            "use_current_date": True,
            "columns": ["time_in_crm"],
            "range": ["2012-01-01 00:00:00", "2020-01-01 00:00:00"],
            "fix": "2018-07-26 01:36",  # '%Y-%m-%d %H:%M:%S'
            "handler": "validators.datetime_validator|DatetimeValidator"

        },
        "budget": {
            "apply": True,
            "apply_fix": True,
            "columns": ["budget_amount_value"],
            "range": [0, 1000000000000],  # todo different for different cols
            "fix": 0,
            "handler": "validators.money_validator|MoneyValidator"
        },
        "fees": {
            "apply": True,
            "apply_fix": True,
            "columns": ["Aval\xfao pagado"],
            "range": [0, 1000000000000],  # todo different for different cols
            "fix": 0,
            "handler": "validators.money_validator|MoneyValidator"
        },
        "none": {
            "apply": False,
            "columns": ["tracking_id"],
            "apply_fix": False,
            "handler": "validators.money_validator|MoneyValidator"

        }
    }
}

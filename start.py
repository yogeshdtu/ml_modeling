import sys
import os
import logging
from datetime import date
from logging import handlers
from models import ml_1_funnel, ml_2_funnel, ml_3_funnel, logistic_regr_entry, logistic_regr_ganado

LOGGER_LEVEL = 10
CWD = os.getcwd()


if __name__ == "__main__":
    logger = logging.getLogger()
    logger.setLevel(level=LOGGER_LEVEL)
    formatter = logging.Formatter(
        '%(asctime)s - %(process)d - %(funcName)s - %(lineno)d - %(levelname)s - %(message)s')

    # Std out
    ch = logging.StreamHandler(sys.stdout)
    ch.setFormatter(formatter)
    ch.setLevel(level=LOGGER_LEVEL)
    logger.addHandler(ch)

    # Log files
    log_directory = CWD + '/logs/'
    if not os.path.exists(log_directory):
        os.makedirs(log_directory)
    fh = handlers.RotatingFileHandler(
        log_directory + 'cj_ml_{}.log'.format(str(date.today())),
        maxBytes=(10240 * 5))
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    logger.info("DECISION TREE")
    logger.info("------------------------>  CASE 1  <------------------------")
    ml_1_funnel.run()
    logger.info("------------------------>  CASE 2  <------------------------")
    ml_2_funnel.run()
    logger.info("------------------------>  CASE 3  <------------------------")
    ml_3_funnel.run()
    logger.info("\n\n\n")
    logger.info("LOGISTIC REGRESSION")
    logger.info("------------------------>  CASE 1  <------------------------")
    logistic_regr_entry.run()
    logger.info("------------------------>  CASE 3  <------------------------")
    logistic_regr_ganado.run()

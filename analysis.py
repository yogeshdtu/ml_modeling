from configs.validators import config
from nltk.corpus import stopwords
import pandas



# todo modify for combination of words, remove stop words
def words_in_query():
    stop_words = set(stopwords.words('spanish'))
    bag_count = {}
    nd_arr = pandas.read_csv(config['filename'], sep=',', usecols=['ga_adMatchedQuery']).values
    for l in nd_arr:
        if not pandas.isnull(l[0]):
            query = l[0]
            query_words = query.strip().split(' ')
            for word in query_words:
                if word not in stop_words:
                    if word in bag_count:
                        bag_count[word] += 1
                    else:
                        bag_count[word] = 1

    ordered_list = []
    for key, value in sorted(bag_count.iteritems(), key=lambda (k, v): (v, k), reverse=True):
        ordered_list.append([key, value])
        #print key, value

    return ordered_list


def correlation():
    # Assuming after removing duplicate data
    # To remove duplicate data, use tracking_id and visit_date as primary key.

    df = pandas.read_csv(config['filename'], sep=',', usecols=["time_in_crm", "Aval\xfao pagado"])
    df.dropna(inplace=True)
    df['time_in_crm'] = pandas.DatetimeIndex(df['time_in_crm']).month
    with pandas.option_context('display.max_rows', 100, 'display.max_columns', 10):
        print df


    return df.corr()


if __name__ == '__main__':
   corr = correlation()
   with pandas.option_context('display.max_rows', 100, 'display.max_columns', 10):
       print(corr)


def getCity():
    df = pandas.read_csv(config['filename'], sep=',', usecols=["tracking_id", "ga_lititude", "ga_longitude", "Aval\xfao pagado"])
    df.dropna(how='any', inplace=True)




